package com.tanitha.week6;

public class BookBankApp {
    public static void main(String[] args) {
        BookBank tanitha = new BookBank("Tanitha", 100.0);
        tanitha.print();
        tanitha.deposit(50);
        tanitha.print();
        tanitha.withdraw(50);
        tanitha.print();
        
        BookBank chanocha = new BookBank("Chanocha", 1);
        chanocha.deposit(1000000);
        chanocha.withdraw(10000000);
        chanocha.print();

        BookBank bigpom = new BookBank("BigPom", 10);
        bigpom.deposit(10000000);
        bigpom.withdraw(1000000);
        bigpom.print();
    }
}
